(function ($) {
    var flatRetinaLogo = function () {
        var retina = window.devicePixelRatio > 1 ? true : false;
        var $logo = $('#logo img');
        var $logo_retina = $logo.data('retina');

        if (retina && $logo_retina) {
            $logo.attr({
                src: $logo.data('retina'),
                width: $logo.data('width'),
                height: $logo.data('height')
            });
        }
    };

    var goTop = function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 800) {
                $('.go-top').addClass('show');
            } else {
                $('.go-top').removeClass('show');
            }
        });

        $('.go-top').on('click', function () {
            $("html, body").animate({ scrollTop: 0 }, 1000, 'easeInOutExpo');
            return false;
        });
    };

    var parallax = function () {
        if ($().parallax && isMobile.any() == null) {
            $('.parallax1').parallax("50%", 1);
            $('.parallax2').parallax("50%", 0.7);
        }
    };

    var removePreloader = function () {
        $(window).on("load", function () {
            $(".loader").fadeOut();
            $("#loading-overlay").delay(500).fadeOut('slow', function () {
                $(this).remove();
            });
        });
    };

    var searchIcon = function () {
        $(document).on('click', function (e) {
            var clickID = e.target.id;
            if ((clickID !== 'input-search')) {
                $('.header-search-form').removeClass('show');
            }
        });

        $('.header-search-icon').on('click', function (event) {
            event.stopPropagation();
        });

        $('.header-search-form').on('click', function (event) {
            event.stopPropagation();
        });

        $('.header-search-icon').on('click', function (event) {
            if (!$('.header-search-form').hasClass("show")) {
                $('.header-search-form').addClass('show');
                event.preventDefault();
            } else
                $('.header-search-form').removeClass('show');
            event.preventDefault();

        });

    };

    var headerFixed = function () {
        if ($('body').hasClass('header_sticky')) {
            var nav = $('#header');

            if (nav.length) {
                var offsetTop = nav.offset().top,
                    headerHeight = nav.height(),
                    injectSpace = $('<div/>', {
                        height: headerHeight
                    }).insertAfter(nav);

                $(window).on('load scroll', function () {
                    if ($(window).scrollTop() > offsetTop) {
                        nav.addClass('is-fixed');
                        injectSpace.show();
                    } else {
                        nav.removeClass('is-fixed');
                        injectSpace.hide();
                    }

                    if ($(window).scrollTop() > 300) {
                        nav.addClass('is-small');

                    } else {
                        nav.removeClass('is-small');
                    }
                });
            }
        };
    };

    var responsiveMenu = function () {

        $('.mobile-button').on('click', function () {

            $('#mainnav-mobi').slideToggle(300);

            $(this).toggleClass('active');

        });



        $(document).on('click', '#mainnav-mobi li .btn-submenu', function (e) {

            $(this).toggleClass('active').next('ul').slideToggle(300);

            e.stopImmediatePropagation()

        });

    };


    // Dom Ready
    $(function () {
        removePreloader();
        goTop();
        parallax();
        flatRetinaLogo();
        searchIcon();
        headerFixed();
        responsiveMenu();
    });
})(jQuery)